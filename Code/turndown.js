let f = () => {
	let cmd = context.previewValues["command"];
	let markdown = context.previewValues["markdown"];
	
	if (!cmd) { return; }
	if (!markdown || markdown.length == 0) { return; }
	
	if (cmd == "copy") {
		app.setClipboard(markdown);
	}
	else if (cmd == "replace") {
		draft.content = markdown;
		draft.update();
	}
	else if (cmd == "new") {
		let d = new Draft();
		d.content = markdown;
		d.update();
		editor.load(d);
	}
}

f();